Announcement template
=====================

A simple Python script to generate an email announcement and a flyer for a seminar or an event.

Usage
-----
From the command line
```shell
python annouce.py --no-flyer seminars/seminar-sample.toml
```

The output is in the `output/` folder. It consists of a sample email and an icalendar event to attach to it.

Example
-------

Sample input in toml format
```toml
title = "An awesome seminar"
author = "John Doe"
affiliation = "Best University (Universe)"
abstract = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non nunc turpis.  Phasellus a ullamcorper leo. Sed fringilla dapibus orci eu ornare. Quisque gravida quam a mi dignissim consequat. Morbi sed iaculis mi. Vivamus ultrices mattis euismod. Mauris aliquet magna eget mauris volutpat a egestas leo rhoncus.
"""
start = "30/01/1980 12:00"
where = "Beautiful room"
```

Email output (rendered from html)

![](https://framagit.org/coslo/template-announcement/-/raw/master/email.png)

Even in icalendar format (to be attached to email)
```
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//df.units.it//seminars//
BEGIN:VEVENT
SUMMARY:Seminar: "An awesome seminar" - John Doe
DTSTART:19800130T120000
DTEND:19800130T130000
LOCATION:Beautiful room
END:VEVENT
END:VCALENDAR
```
