"""
Script to generate 

- email announcement
- icalendar event 
- flyer

for a seminar from a toml or yaml file.
"""

import sys
import os
import copy
from jinja2 import Environment, FileSystemLoader
from icalendar import Calendar, Event, vCalAddress, vText, Alarm
from datetime import datetime, timedelta
from pathlib import Path
from helpers import db_loader


def db_loader(path):
    """
    Load a database file `path` into a dictionary.

    Accept yaml and toml formats.
    """
    ext = os.path.splitext(path)[1]
    if ext == '.yaml':
        import yaml
        with open(path) as fh:
            db = yaml.load(fh)

    elif ext == '.toml':
        import tomli
        with open(path, "rb") as fh:
            db = tomli.load(fh)

    else:
        raise ValueError(f'unsupported database file {path}')

    # Load additional data
    date = datetime.strptime(db['start'], '%d/%m/%Y %H:%M')
    date_end = copy.copy(date)
    date_end += timedelta(hours=1)
    db['date'] = date
    db['date_end'] = date_end
    db['day_of_week'] = date.strftime('%A')
    db['month'] = date.strftime('%B')
    db['day'] = date.day
    db['year'] = date.year
    db['hour_start'] = date.strftime('%H:%M')
    db['hour_end'] = date_end.strftime('%H:%M')
    assert 'contact' in db
    return db

def email(db, outdir, template):
    """Interpolate the email template"""
    # Write the email body
    environment = Environment(loader=FileSystemLoader(['.']))
    template = environment.get_template(template)
    page = template.render(**db)
    with open(os.path.join(outdir, 'email.html'), 'w') as fh:
        fh.write(page)

    # Write the subject
    template = environment.get_template('templates/subject-template.txt')
    page = template.render(**db)
    with open(os.path.join(outdir, 'subject.txt'), 'w') as fh:
        fh.write(page)
    return page

def event(db, outdir, alarm=False, description=False):
    # Create and add the event to the calendar
    cal = Calendar()
    cal.add('prodid', '-//df.units.it//seminars//')
    cal.add('version', '2.0')
    cal.add('method', 'PUBLISH')
    cal.add('calscale', 'GREGORIAN')
    event = Event()
    event.add('summary', '"{title}" - {speaker}'.format(**db))
    event.add('dtstart', db['date'])
    event.add('dtend', db['date_end'])
    event.add('categories', vText('Seminar'))
    if description:
        event.add('description', r"{abstract}".format(**db))

    if alarm:
        a = Alarm()
        a.add('trigger', timedelta(minutes=-10))
        a.add('action', 'DISPLAY')
        a.add('description', 'Reminder')
        event.add_component(a)

    # # Add the organizer
    # organizer = vCalAddress('MAILTO:jdoe@example.com') 
    # organizer.params['name'] = vText('John Doe')
    # event['organizer'] = organizer
    event['location'] = vText('{where}'.format(**db))
    cal.add_component(event)

    # Write the event in icalendar format
    with open(os.path.join(outdir, 'seminar.ics'), 'wb') as fh:
        fh.write(cal.to_ical())
    return cal

def flyer(db, outdir, template):
    """
    Render flyer in odg format.

    It requires an open libreoffice instance:
    $ soffice --accept='socket,host=localhost,port=8100;urp;StarOffice.Service'
    """
    from unotools.errors import ConnectionError
    from unotools import Socket, connect
    from unotools.component.writer import Writer
    from unotools.unohelper import convert_path_to_url

    # subprocess.call(['soffice', "--accept='socket,host=localhost,port=8100;urp;StarOffice.Service'", "&"], shell=True)

    try:
        context = connect(Socket('localhost', 8100))
    except ConnectionError:
        print("First run\n$ soffice --accept='socket,host=localhost,port=8100;urp;StarOffice.Service'")
        sys.exit(1)

    writer = Writer(context, convert_path_to_url(template))
    page = writer.getDrawPages().getByIndex(0)
    for i in range(page.getCount()):
        element = page.getByIndex(i)
        if len(element.String) > 0:
            txt = element.String
            for key in db:
                if f'<{key}>' in element.String:
                    import re
                    txt = re.sub(f'<{key}>', str(db[key]), txt)
            element.setString(txt)
    url = convert_path_to_url(os.path.join(outdir, 'flyer.odg'))
    writer.store_to_url(url, 'FilterName', 'writer8')
    # url = convert_path_to_url(os.path.join(outdir, 'flyer.pdf'))
    # writer.store_to_url(url, 'FilterName', 'writer_pdf_Export')
    writer.close(True)


def main(seminar_file, no_flyer=False,
         template_email='templates/email-template.html',
         template_flyer='templates/flyer-template-units.odg'):
    
    db = db_loader(seminar_file)

    outdir = Path.cwd() / 'output'
    outdir.mkdir(parents=True, exist_ok=True)
    
    email(db, outdir, template_email)
    event(db, outdir)
    if not no_flyer:
        flyer(db, outdir, template_flyer)


if __name__ == '__main__':
    import argh
    argh.dispatch_command(main)
    
